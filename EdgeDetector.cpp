/**
 * Data transmission format
 * $xxxxyyyytttt*
 * xxxx - x position no decimal in mm
 * yyyy - y position no decimal in mm
 * tttt - time 2 decimal in seconds
 */

#include <opencv2/imgproc/imgproc.hpp>
//#include <opencv2/imgcodecs.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/video/tracking.hpp>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <limits>

using namespace cv;

#define PIXELTOMM 1.4 //Converts the velocity from camera space to real world
#define MMTOPIXEL 0.71428571428

/// Global variables

Mat src, src_gray;
Mat dst, detected_edges;

int edgeThresh = 1;
int lowThreshold;
int const max_lowThreshold = 100;
int ratio = 3;
int kernel_size = 3;
const char* window_name = "Edge Map";

Point2d pos;
Point2d prevPos;
Point2d vel;
Point2d prevVel;
Point2d cameraOff(12.7, 44.45); //offset from edge of camera to paddle (0,0). (Subtract off)
//Point cameraOff(10.0, 40.0);
double lookAhead = 3.0;

int minRad = 15;    //Radius of the puck
int maxRad = 25; 
int jitter = 9;

double cTime;
double cTimePrev;

Point2d goal;
double gTime;
Point2d min, max;

int eLowH = 85;
int eHighH = 114;
int eLowS = 120; 
int eHighS = 167;
int eLowV = 0;
int eHighV = 95;

int pLowH = 23;
int pHighH = 31;
int pLowS = 149; 
int pHighS = 230;
int pLowV = 103;
int pHighV = 133;

bool pause = false;

/**
 * @function CannyThreshold
 * @brief Trackbar callback - Canny thresholds input with a ratio 1:3
 */
static void CannyThreshold(int, void*)
{
    /// Reduce noise with a kernel 3x3
    blur( src_gray, detected_edges, Size(3,3) );

    /// Canny detector
    Canny( detected_edges, detected_edges, lowThreshold, lowThreshold*ratio, kernel_size );

    /// Using Canny's output as a mask, we display our result
    dst = Scalar::all(0);

    src.copyTo( dst, detected_edges);
    imshow( window_name, dst );
}

std::vector<Point2d> kalmanp;
std::vector<Point2d> puckp;
std::vector<Point2d> puckv;
std::vector<Point2d> bouncePath;
/**
 * @function main
 */
int main( int, char** argv ) {
    VideoCapture cap(1); //capture the video from webcam

    KalmanFilter kf(4, 2, 0);
    kf.transitionMatrix = *(Mat_<float>(4, 4) << 1,0,1,0,   0,1,0,1,  0,0,1,0,  0,0,0,1);
    Mat_<float> measurement(2,1); 
    measurement.setTo(Scalar(0));
    // init...
    kf.statePre.at<float>(0) = 476;
    kf.statePre.at<float>(1) = 476;
    kf.statePre.at<float>(2) = 0;
    kf.statePre.at<float>(3) = 0;
    setIdentity(kf.measurementMatrix);
    setIdentity(kf.processNoiseCov, Scalar::all(1e-4));
    setIdentity(kf.measurementNoiseCov, Scalar::all(1e-1));
    setIdentity(kf.errorCovPost, Scalar::all(.1));

    if (!cap.isOpened())  { // if not success, exit program
         std::cout << "Cannot open the web cam" << std::endl;
         return -1;
    }
    cTimePrev = (double)getTickCount(); 
    /// Load an image
    Mat imgTmp;
    cap.read(imgTmp); 
    
    while(true) {
        if(pause) {
            if(waitKey(0)) {
                pause = false;
            }
        }
        bool bSuccess = cap.read(src); // read a new frame from video

        if (!bSuccess) { //if not success, break loop
             std::cout << "Cannot read a frame from video stream" << std::endl;
             break;
        }
        //imshow("Source", src);
        
        bouncePath.clear();
        
        createTrackbar("LowH", "Threshold", &eLowH, 179); //Hue (0 - 179)
        createTrackbar("HighH", "Threshold", &eHighH, 179);
        createTrackbar("LowS", "Threshold", &eLowS, 255); //Saturation (0 - 255)
        createTrackbar("HighS", "Threshold", &eHighS, 255);
        createTrackbar("LowV", "Threshold", &eLowV, 255);//Value (0 - 255)
        createTrackbar("HighV", "Threshold", &eHighV, 255);
        
        createTrackbar("LowH", "Puck", &pLowH, 179); //Hue (0 - 179)
        createTrackbar("HighH", "Puck", &pHighH, 179);
        createTrackbar("LowS", "Puck", &pLowS, 255); //Saturation (0 - 255)
        createTrackbar("HighS", "Puck", &pHighS, 255);
        createTrackbar("LowV", "Puck", &pLowV, 255);//Value (0 - 255)
        createTrackbar("HighV", "Puck", &pHighV, 255);
        
        createTrackbar("Min Radius", "Lines", &minRad, 40);//Value (0 - 255)
        createTrackbar("Max Radius", "Lines", &maxRad, 40);
        createTrackbar("Jitter", "Lines", &jitter, 20);
        
        /// Create a matrix of the same type and size as src (for dst)
        dst.create( src.size(), src.type() );
        Mat imgThresholded;
        Mat imgHSV;
        cvtColor(src, imgHSV, COLOR_BGR2HSV);
        
        inRange(imgHSV, Scalar(eLowH, eLowS, eLowV), Scalar(eHighH, eHighS, eHighV), src_gray); //Threshold the image
        
        inRange(imgHSV, Scalar(pLowH, pLowS, pLowV), Scalar(pHighH, pHighS, pHighV), imgThresholded); //Threshold the image
        GaussianBlur( imgThresholded, imgThresholded, Size(11, 11), 0, 0 );
        imshow("Puck", imgThresholded);

        /// Convert the image to grayscale
        imshow("Threshold", src_gray);
        
        
        Mat prediction = kf.predict();
        Point predictPt(prediction.at<float>(0),prediction.at<float>(1));
        
        /// HoughLines
        Mat cdst;
        cvtColor(dst, cdst, COLOR_HSV2BGR);
        vector<Vec4i> lines;
        HoughLinesP(src_gray, lines, 1, CV_PI/180, 10, 50, 20 );
        for( size_t i = 0; i < lines.size(); i++ )
        {
          Vec4i l = lines[i];
          line( src, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(0,0,255), 1, CV_AA);
        }
        /// HoughCircle
        std::vector<Vec3f> circles;
        HoughCircles( imgThresholded, circles, CV_HOUGH_GRADIENT, 1, imgThresholded.rows/8, 400, 15, 0, 0 );
        if(circles.size() == 0) {
            printf("No Puck\n");
            prevPos.x = prevPos.y = std::numeric_limits<double>::min();
            pos.x = pos.y = std::numeric_limits<double>::min();
            prevVel.x = prevVel.y = std::numeric_limits<double>::min();
            //imshow("Lines", src);
            kf.statePost = prediction;
            //continue; //If no previous data, skip to next frame
        }
        for( size_t i = 0; i < circles.size(); i++ )
        {
            if(circles[i][2] < minRad || circles[i][2] > maxRad) {
                printf("Puck outside of radius: %f\n", circles[i][2]);
                continue;
            }
            prevPos = pos;
            pos = Point2d(circles[i][0]*PIXELTOMM, circles[i][1]*PIXELTOMM);
            pos = pos - cameraOff;
            if(prevPos.x == std::numeric_limits<double>::min()) {
                prevPos = pos;
            }
            if(norm(pos-prevPos) < jitter/4.0) {
                pos = prevPos;
            }
            measurement(0) = pos.x;
            measurement(1) = pos.y;
            Point center(cvRound(circles[i][0]), cvRound(circles[i][1]));
            int radius = cvRound(circles[i][2]);
            // circle center
            circle( src, center, 3, Scalar(0,255,0), -1, 8, 0 );
            // circle outline
            circle( src, center, radius, Scalar(255,0,0), 3, 8, 0 );
        }
        //printf("Circles: %d\n", circles.size());
        //printf("Lines: %d\n", lines.size());
        Mat estimated = kf.correct(measurement);
        Point2d statePt(estimated.at<float>(0),estimated.at<float>(1));
        Point2d measPt(measurement(0), measurement(1));
        
        cTime = (double)getTickCount(); 
        double dt = (cTime - cTimePrev) / getTickFrequency();
        cTimePrev = cTime;
        
        if(circles.size() == 0) {
            imshow("Lines", src);
            //printf("Continuing\n\n");
            continue;
        }
        /// Estimate velocity
        prevVel = vel;
        Point2d dist = pos - prevPos;
        printf("Dist: (%f, %f) - (%f, %f) = (%f, %f)\n", pos.x, pos.y, prevPos.x, prevPos.y, dist.x, dist.y);
        Point2d vEst = dist * (1.0/dt);
        if(prevVel.x == std::numeric_limits<double>::min()) {
            vel = vEst; //No previous data
        }
        else {
            /// Average it with the previous
            vel = (vEst + prevVel) * 0.5;
            //vel = vEst;
        }
        
        /// Extrapolate velocity up to lookAhead
        Point2d start = pos;
        Point2d end = start + vel * lookAhead;
        bouncePath.push_back(start);
        bool canBounce = true;
        int c = 0;
        /// Check for collisions along the trajectory and account for bounce
        while(true) {
            printf("Loop: %d\n", ++c);
            printf("Start: (%.15g, %.15g)\nEnd: (%.15g, %.15g)\n", start.x, start.y, end.x, end.y);
            printf("Vel: (%f, %f)\n", vel.x, vel.y);
            bool noIntersections = true;
            Point2d segS, segE;
            double a = 2, b = 2; //t, u
            Point2d r = end - start;
            for(int i = 0; i < lines.size(); i++) {
                Point2d lS = Point2d(lines[i][0], lines[i][1]);
                lS = lS * PIXELTOMM - cameraOff;
                Point2d lE = Point2d(lines[i][2], lines[i][3]);
                lE = lE * PIXELTOMM - cameraOff;
                Point2d s = lE - lS;
                double denom = (lE.y-lS.y) * (end.x-start.x) - (lE.x-lS.x) * (end.y-start.y);
                if(denom > 0.0001) {
                    double ua = (lE.x-lS.x)*(start.y-lS.y) - (lE.y-lS.y)*(start.x-lS.x);
                    ua /= denom;
                    double ub = (end.x-start.x)*(start.y-lS.y) - (end.y-start.y)*(start.x-lS.x);
                    ub /= denom;
                    if(ua >= 0 && ua <= 1 && ub >= 0 && ub <= 1) {
                        if(ua < a) {
                            segS = lS;
                            segE = lE;
                            a = ua;
                            b = ub;
                            noIntersections = false;
                        }
                    }
                }
            }
            if(noIntersections) {
                bouncePath.push_back(end);
                printf("No intersections\n\n");
                break;
            }
            //printf("Intersect\n");
            //intersect at p+tr = q+us
            Point2d s = segE - segS;
            Point2d insect = start + a * r;
            printf("Intersect Point: (%f, %f)\n", insect.x, insect.y);
            bouncePath.push_back(insect);
            //find normal vector
            double lsq = 1.0 / (norm(s) * norm(s));
            Point2d n = r - (r.ddot(s)*lsq * s);
            //line(src, (insect+cameraOff)*MMTOPIXEL, (insect-n+cameraOff)*MMTOPIXEL, Scalar(0, 255, 255), 5);
            n = (-n) * (1.0/norm(n));
            //reflect vector
            //r = -r;
            Point2d v = 2*((-r).ddot(n))*n + r;
            v = (v * (1.0/norm(v))) * (norm(r)*(1-a)); 
            start = insect;
            end = insect + v;
            printf("New Start: (%f, %f)\nNew End: (%f, %f)\n", start.x, start.y, end.x, end.y);
            printf("Norm: %f\n\n", norm(v));
            if(norm(v) < 5) {
                //end bouncing
                bouncePath.push_back(end);
                canBounce = false;
            }
            if(!canBounce) {
                break;
            }
        }
        puckp.push_back(measPt);
        kalmanp.push_back(statePt);
        puckv.push_back(vel);
        for(int i = 0; i < puckp.size()-1; i++) {
            line(src, (puckp[i]+cameraOff)*MMTOPIXEL, (puckp[i+1]+cameraOff)*MMTOPIXEL, Scalar(0, 255, 255), 1);
            line(src, (kalmanp[i]+cameraOff)*MMTOPIXEL, (kalmanp[i+1]+cameraOff)*MMTOPIXEL, Scalar(0, 0, 255), 1);
            //line(src, (puckp[i]+cameraOff)*MMTOPIXEL, (puckp[i]+0.25*puckv[i]+cameraOff)*MMTOPIXEL, Scalar(255, 0, 0), 1);
        }
        /// Draw Projected Path
        printf("Bounce Size: %d\n", (int)bouncePath.size());
        for(int i = 0; i < bouncePath.size()-1; i++) {
            line(src, (bouncePath[i]+cameraOff)*MMTOPIXEL, (bouncePath[i+1]+cameraOff)*MMTOPIXEL, Scalar(255, 255, 0), 1);
        }
        
        //printf("Start: (%f, %f), End: (%f, %f)\n", pos[0], pos[1], end[0], end[1]);
        //printf("Vel: (%f, %f)\n", vel[0], vel[1]);
        imshow("Lines", src);
        
        /// For now, just make it follow the puck along the y axis
        goal.y = 100;
        goal.x = pos.x;
        
        /// Convert to format
        /// std::stringstream output;
        /// ss << "$" << setw(4) << setfill('0') << cvRound(goal.x);
        /// ss << setw(4) << setfill('0') << cvRound(goal.y);
        /// int modTime = cvRound(gTime*100);
        /// ss << setw(4) << setfill('0') << modTime;
        /// ss << "*";

        /// Wait until user exit program by pressing a key
        int code;
        if((code = waitKey(30)) > -1) {
            printf("Code: %d, %c\n", code%256, code%256);
            if(code%256 == 'p') {
                pause = true;
            }
            else {
                std::cout << "key is pressed by user" << std::endl;
                //break;
                std::exit(0); 
            }
        }
    }

    return 0;
}
